#!/usr/bin/env python
# encoding: utf-8
# Date: 2017/2/4 下午1:16
# file: mqtt_client.py
# Email: wangjian2254@icloud.com
# Author: 王健
from time import sleep


def run_by_command():
    try:
        import thread
        from mqtt.mqtt_client import mqtt_gateway_client_run

        thread.start_new_thread(mqtt_gateway_client_run, ('opc_',))
        sleep(5)
    except Exception as e:
        print str(e)
